#!/bin/bash
cd /root
wget -q --spider http://google.com
if [ $? -eq 0 ]; then
	echo "Online"
	wget -q https://s3-us-west-1.amazonaws.com/download.sapho.com/sapho-${SAPHO_VERSION}.zip -O sapho-latest.zip
else
	echo "Offline"
fi

unzip -jo sapho-latest.zip -d sapho-latest
rm sapho-latest.zip
rm -rf /${CATALINA_HOME}/webapps/ROOT && rm -rf /${CATALINA_HOME}/webapps/docs
rm -rf /${CATALINA_HOME}/webapps/examples && rm -rf /${CATALINA_HOME}/webapps/host-manager
rm -rf /${CATALINA_HOME}/webapps/manager
mv sapho-latest sapho
echo ${SAPHO_VERSION} > /root/sapho/sapho-version.txt
cp sapho/sapho.war ${CATALINA_HOME}/webapps/ROOT.war
printf '#!/bin/bash\nJAVA_OPTS="-Xms1024m -Xmx2048m -XX:MaxPermSize=256m"\nCATALINA_OPTS="-DSAPHO_DB_HOSTNAME=${SAPHO_DB_HOSTNAME} -DSAPHO_DB_USERNAME=${SAPHO_DB_USERNAME} -DSAPHO_DB_PASSWORD=${SAPHO_DB_PASSWORD} -DSAPHO_DB_NAME=${SAPHO_DB_NAME} -DSAPHO_DB_TYPE=${SAPHO_DB_TYPE} -DSAPHO_DB_PORT=${SAPHO_DB_PORT} -DSAPHO_HEADLESS=${SAPHO_HEADLESS} -DSAPHO_VERSION=${SAPHO_VERSION} -Djava.awt.headless=true -XX:+UseConcMarkSweepGC"\n' > ${CATALINA_HOME}/bin/setenv.sh
chmod +x ${CATALINA_HOME}/bin/setenv.sh
cd ${CATALINA_HOME}/bin/
catalina.sh run